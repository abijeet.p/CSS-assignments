## Work - Session 2

### Task 2.1 

A start up company needs a website for its services. Design a website as shown below. Use the file names same as shown in the image. When we click on the logo it should redirect to home page.

#### Home Page 

![Home Page](img/home.png)

#### About Us Page 

![About Us] (img/about-us.png)

#### Contact Us Page

![Contact Us] (img/contact-us.png)

**Note:**

* The font family should be "Open Sans".
* The logo should be background image for the div.
* The text should be any content which includes links, italic text, underlined text etc.
* When we click on the menu links on the left side, its color should be changed to red. When we hover on the link it should change its color.
* You can use any color(s) for the website. 

### Task 2.2

Design the Menu for the web page as shown in the below image. Design the web page with the same colors used in the below image. Use only hexacodes for colors.

![Task 2.2](img/Menu.png)

The menu items color should be changed to red when we hover on it. 


**NOT REQURED :: If the menu item has sub menu, then it should display the list when we hover on main menu item.**

### Task 2.3

Design a html page as shown below. Use same colors as shown in image. Use CSS resetter before writing your CSS.

![Task 2.3](img/task2.3.png)