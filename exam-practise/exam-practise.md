Please markup and design the following pages.

**Read the comments in the images carefully**

## Homepage

![](https://gitlab.com/trainee.batch7_admin/CSS-assignments/raw/master/exam-practise/homepage.png)

## Contact Us 

![](https://gitlab.com/trainee.batch7_admin/CSS-assignments/raw/master/exam-practise/contact-us.png)